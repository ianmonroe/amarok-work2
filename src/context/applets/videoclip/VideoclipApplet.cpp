/****************************************************************************************
 * Copyright (c) 2009 Simon Esneault <simon.esneault@gmail.com>                         *
 *                                                                                      *
 * This program is free software; you can redistribute it and/or modify it under        *
 * the terms of the GNU General Public License as published by the Free Software        *
 * Foundation; either version 2 of the License, or (at your option) any later           *
 * version.                                                                             *
 *                                                                                      *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY      *
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A      *
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.             *
 *                                                                                      *
 * You should have received a copy of the GNU General Public License along with         *
 * this program.  If not, see <http://www.gnu.org/licenses/>.                           *
 ****************************************************************************************/

//Plasma applet for showing videoclip in the context view

#include "VideoclipApplet.h" 

#include "CustomVideoWidget.h"
#include "PaletteHandler.h"
#include "VideoItemButton.h"

// Amarok
#include "core/support/Amarok.h"
#include "core/support/Debug.h"
#include "EngineController.h"
#include "core-impl/meta/stream/Stream.h"
#include "core-impl/collections/support/CollectionManager.h"
#include "context/ContextView.h"
#include "context/Svg.h"
#include "playlist/PlaylistModelStack.h"
#include "SvgHandler.h"
#include "widgets/kratingpainter.h"
#include "widgets/kratingwidget.h"
#include "widgets/TextScrollingWidget.h"

// KDE
#include <KColorScheme>
#include <KConfigDialog>
#include <KStandardDirs>
#include <KVBox>
#include <Plasma/Theme>
#include <Plasma/BusyWidget>
#include <Plasma/IconWidget>
#include <Phonon/MediaObject>
#include <Phonon/Path>
#include <Phonon/VideoWidget>

// Qt
#include <QGraphicsLinearLayout>
#include <QGraphicsProxyWidget>
#include <QGraphicsWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPainter>
#include <QToolButton>
#include <QScrollArea>
#include <QScrollBar>

#define DEBUG_PREFIX "VideoclipApplet"




Q_DECLARE_METATYPE ( VideoInfo *)
K_EXPORT_AMAROK_APPLET( videoclip, VideoclipApplet )


VideoclipApplet::VideoclipApplet( QObject* parent, const QVariantList& args )
        : Context::Applet( parent, args )
        , Engine::EngineObserver( The::engineController() )
        , m_settingsIcon( 0 )
        , m_youtubeHQ( false )
{
    DEBUG_BLOCK
    setHasConfigurationInterface( true );
}

void 
VideoclipApplet::init()
{
    // Call the base implementation.
    Context::Applet::init();

    setBackgroundHints( Plasma::Applet::NoBackground );

    m_height = 300;
    resize( 300, -1 );

    // CustomWidget is a special VideoWidget for interaction
    m_videoWidget = new CustomVideoWidget();
    m_videoWidget.data()->setParent( Context::ContextView::self()->viewport(), Qt::SubWindow | Qt::FramelessWindowHint );
    m_videoWidget.data()->hide();
    
    // we create path no need to add a lot of fancy thing 
    Phonon::createPath( const_cast<Phonon::MediaObject*>( The::engineController()->phononMediaObject() ), m_videoWidget.data() );

    
    // Load pixmap
    m_pixYoutube = new QPixmap( KStandardDirs::locate( "data", "amarok/images/amarok-videoclip-youtube.png" ) );
    m_pixDailymotion = new QPixmap( KStandardDirs::locate( "data", "amarok/images/amarok-videoclip-dailymotion.png" ) );
    m_pixVimeo = new QPixmap( KStandardDirs::locate( "data", "amarok/images/amarok-videoclip-vimeo.png" ) );

    QAction* langAction = new QAction( this );
    langAction->setIcon( KIcon( "preferences-system" ) );
    langAction->setVisible( true );
    langAction->setEnabled( true );
    langAction->setText( i18n( "Settings" ) );
    m_settingsIcon = addAction( langAction );
    connect( m_settingsIcon, SIGNAL( clicked() ), this, SLOT( showConfigurationInterface() ) );

    
    // Create label
    QFont labelFont;
    labelFont.setPointSize( labelFont.pointSize() + 2 );
    m_headerText = new TextScrollingWidget( this );
    m_headerText->setBrush( Plasma::Theme::defaultTheme()->color( Plasma::Theme::TextColor ) );
    m_headerText->setFont( labelFont );
    m_headerText->setText( i18n( "Video Clip" ) );

    // Set the collapse size
    setCollapseHeight( m_headerText->boundingRect().height() + 3 * standardPadding() );

    // Create layout
    m_layout = new QHBoxLayout();
    m_layout->setSizeConstraint( QLayout::SetMinAndMaxSize );
    m_layout->setContentsMargins( 5, 5, 5, 5 );
    m_layout->setSpacing( 2 );

    // create a widget
    QWidget *window = new QWidget;
    window->setAttribute( Qt::WA_NoSystemBackground );
    window->setLayout( m_layout );

    // create a scroll Area
    m_scroll = new QScrollArea();
    m_scroll->setMaximumHeight( m_height - m_headerText->boundingRect().height() - 4*standardPadding() );
    m_scroll->setWidget( window );
    m_scroll->setAttribute( Qt::WA_NoSystemBackground );
    m_scroll->viewport()->setAttribute( Qt::WA_NoSystemBackground );

    m_widget = new QGraphicsProxyWidget( this );
    m_widget->setWidget( m_scroll );
    m_widget->hide();

    QGraphicsLinearLayout *headerLayout = new QGraphicsLinearLayout;
    headerLayout->addItem( m_headerText );
    headerLayout->addItem( m_settingsIcon );
    headerLayout->setContentsMargins( 0, 4, 0, 2 );

    QGraphicsLinearLayout *layout = new QGraphicsLinearLayout( Qt::Vertical, this );
    layout->addItem( headerLayout );
    layout->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );

    constraintsEvent();
    
    //Update the applet (render properly the header)
    update();

    connectSource( "videoclip" );
    
    connect( dataEngine( "amarok-videoclip" ), SIGNAL( sourceAdded( const QString & ) ),
             this, SLOT( connectSource( const QString & ) ) );

    engineStateChanged(Phonon::PlayingState,Phonon::StoppedState);// kickstart

    // Read config and inform the engine.
    KConfigGroup config = Amarok::config("Videoclip Applet");
    m_youtubeHQ = config.readEntry( "YoutubeHQ", false );
    dataEngine( "amarok-videoclip" )->query( QString( "videoclip:youtubeHQ:" ) + QString().setNum( m_youtubeHQ ) );
    setCollapseOn();
}

VideoclipApplet::~VideoclipApplet()
{
    DEBUG_BLOCK
   
    delete m_videoWidget.data();
    qDeleteAll( m_videoItemButtons );
}

void 
VideoclipApplet::engineNewTrackPlaying()
{
    DEBUG_BLOCK
    // on new track, we expand the applet if not already
    setCollapseOff();
    static_cast<QGraphicsLinearLayout*>( layout() )->addItem( m_widget );
    m_videoWidget.data()->show();
    setMinimumHeight( 300 );
    emit sizeHintChanged( Qt::MinimumSize );
    layout()->invalidate();
}

void
VideoclipApplet::engineStateChanged(Phonon::State currentState, Phonon::State oldState)
{
    DEBUG_BLOCK

    debug() << "video old state: " << oldState << " new state: " << currentState;

    if ( currentState == oldState )
        return;

    switch ( currentState )
    {
        // when switching from buffering to to playing, we launch the vid widget
        case Phonon::PlayingState :
        {
            // We need this has when song switching the state will do
            // playing > stopped > playing > loading > buffering > playing

            // --well, not on OS X. there it will go oldState == stopped,
            // newState == playing, after which the track actually starts playing
            // adding StoppedState below makes the videoapplet work for video podcasts.
            // I suggest adding a special case for OS X if this breaks on other platforms - max
            if ( oldState == Phonon::BufferingState || oldState == Phonon::StoppedState )
            {
                debug() <<" video state : playing";

                if ( The::engineController()->phononMediaObject()->hasVideo() )
                {
                    setBusy( false );
                    debug() << " VideoclipApplet | Show VideoWidget";
                    m_widget->hide();
                    m_videoWidget.data()->show();
                    m_videoWidget.data()->activateWindow();
                    Phonon::createPath( const_cast<Phonon::MediaObject*>( The::engineController()->phononMediaObject() ), m_videoWidget.data() );
                    if( m_videoWidget.data()->isActiveWindow() ) {
                        //FIXME dual-screen this seems to still show
                        QContextMenuEvent e( QContextMenuEvent::Other, QPoint() );
                        QApplication::sendEvent( m_videoWidget.data(), &e );
                    }
                }
                else
                {
                    debug() << " VideoclipApplet | Hide VideoWidget";
                    m_videoWidget.data()->hide();
                }
            }
            break;
        }

        // When buffering/loading  a vid, we want the nice Busy animation, and no collapsing
        case Phonon::BufferingState:
        case Phonon::LoadingState:
        {
            debug() <<" video state : buffering";

            setBusy( true );
            m_videoWidget.data()->hide();
            m_widget->hide();
            break;
        }

        default:
            break;
    }
}

void 
VideoclipApplet::enginePlaybackEnded( qint64 finalPosition, qint64 trackLength, PlaybackEndedReason reason )
{
    Q_UNUSED( finalPosition )
    Q_UNUSED( trackLength )
    Q_UNUSED( reason )

    // On playback ending, we hide everything and collapse
    setBusy( false );
    m_widget->hide();
    m_videoWidget.data()->hide();
    static_cast<QGraphicsLinearLayout*>( layout() )->removeItem( m_widget );
    setCollapseOn();

}

void 
VideoclipApplet::constraintsEvent( Plasma::Constraints constraints )
{
    Q_UNUSED( constraints );
    prepareGeometryChange();

    m_headerText->setScrollingText( i18n( "Video Clip" ) );
}

void 
VideoclipApplet::paintInterface( QPainter *p, const QStyleOptionGraphicsItem *option, const QRect &contentsRect )
{
    Q_UNUSED( option );
    Q_UNUSED( contentsRect );
    p->setRenderHint( QPainter::Antialiasing );
    // tint the whole applet
    addGradientToAppletBackground( p );
    // draw rounded rect around title
    drawRoundedRectAroundText( p, m_headerText );
}

void 
VideoclipApplet::connectSource( const QString &source )
{
    if ( source == "videoclip" )
        dataEngine( "amarok-videoclip" )->connectSource( "videoclip", this );
}

void 
VideoclipApplet::dataUpdated( const QString& name, const Plasma::DataEngine::Data& data ) // SLOT
{
    DEBUG_BLOCK
    Q_UNUSED( name )

    if ( data.empty() )
        return;
    
    if ( !m_videoWidget.data()->isVisible() && !The::engineController()->phononMediaObject()->hasVideo() )
    {
        int width = 130;
        // Properly delete previsouly allocated item
        while ( !m_layoutWidgetList.empty() )
        {
            m_layoutWidgetList.front()->hide();
            m_layout->removeWidget( m_layoutWidgetList.front() );
			delete m_layoutWidgetList.front();
            m_layoutWidgetList.pop_front();            
        }
        
        // if we get a message, show it
        if ( data.contains( "message" ) && data["message"].toString().contains("Fetching"))
        {
            m_headerText->setText( i18n( "Video Clip" ) );
            constraintsEvent();
            update();
            debug() <<" message fetching ";
            m_widget->hide();
            static_cast<QGraphicsLinearLayout*>( layout() )->removeItem( m_widget );
			setBusy( true );
        }
		else if ( data.contains( "message" ) )
		{
            //if nothing found, we collapse and inform user
            m_headerText->setText( i18n( "Video Clip " ) + ':' + i18n( " No information found..." ) );
            update();
			setBusy( false );
            m_widget->hide();
            static_cast<QGraphicsLinearLayout*>( layout() )->removeItem( m_widget );
            setCollapseOn();
		}
        else if ( data.contains( "item:0" ) )
        {
            m_headerText->setText( i18n( "Video Clip" ) );
            update();
            // set collapsed
            // tint the applet
            m_widget->show();

			setBusy(false);
            for (int i=0; i< data.size(); i++ )
            {
                VideoInfo *item = data[ QString ("item:" )+QString().setNum(i) ].value<VideoInfo *>() ;
                if( !( item->url.isEmpty() ) ) // prevent some weird stuff ...
                {
                    VideoItemButton *vidButton = new VideoItemButton();
                    vidButton->setVideoInfo( item );
                    m_videoItemButtons.append( vidButton );

                    connect ( vidButton, SIGNAL( appendRequested( VideoInfo * ) ), this, SLOT ( appendVideoClip( VideoInfo * ) ) );
                    connect ( vidButton, SIGNAL( queueRequested( VideoInfo* ) ), this, SLOT ( queueVideoClip( VideoInfo * ) ) );
                    connect ( vidButton, SIGNAL( appendPlayRequested( VideoInfo * ) ), this, SLOT ( appendPlayVideoClip( VideoInfo * ) ) );

                    // create link (and resize, no more than 3 lines long)
                    QString title( item->title );
                    if ( title.size() > 45 ) title.resize( 45 );
                    QLabel *link = new QLabel( QString( "<html><body><a href=\"" ) + item->url + QString( "\">" ) + title + QString( "</a>" ) );
                    link->setOpenExternalLinks( true );
                    link->setWordWrap( true );

                    QLabel *duration =  new QLabel( item->duration + QString( "<br>" ) + item->views + QString( " views" ) );

                    Amarok::KRatingWidget* rating = new Amarok::KRatingWidget;
                    rating->setRating(( int )( item->rating * 2. ) );
                    rating->setMaximumWidth(( int )(( width / 3 )*2 ) );
                    rating->setMinimumWidth(( int )(( width / 3 )*2 ) );

                    QLabel *webi = new QLabel;
                    if ( item->source == QString( "youtube" ) )
                        webi->setPixmap( *m_pixYoutube );
                    else if ( item->source == QString( "dailymotion" ) )
                        webi->setPixmap( *m_pixDailymotion );
                    else if ( item->source == QString( "vimeo" ) )
                        webi->setPixmap( *m_pixVimeo );


                    QGridLayout *grid = new QGridLayout();
                    grid->setHorizontalSpacing( 5 );
                    grid->setVerticalSpacing( 2 );
                    grid->setRowMinimumHeight( 1, 65 );
                    grid->setColumnStretch( 0, 0 );
                    grid->setColumnStretch( 1, 1 );
                    grid->addWidget( vidButton, 0, 0, 1, -1, Qt::AlignCenter );
                    grid->addWidget( link, 1, 0, 1, -1, Qt::AlignCenter | Qt::AlignTop );
                    grid->addWidget( webi, 2, 0, Qt::AlignCenter );
                    grid->addWidget( duration, 2, 1, Qt::AlignLeft );
                    grid->addWidget( rating, 3, 0, 1, -1, Qt::AlignCenter );

                    // Add The Widget
                    QWidget *widget = new QWidget();
                    widget->setLayout( grid );
                    widget->resize( width, m_height - m_headerText->boundingRect().height() - 2*standardPadding() );
                    widget->setMaximumWidth( width );
                    widget->setMinimumWidth( width );
                    widget->setMinimumHeight( m_height - ( m_headerText->boundingRect().height() + 10 * standardPadding() ) );
                    widget->setMaximumHeight( m_height - ( m_headerText->boundingRect().height() + 10 * standardPadding() ) );
                    m_layout->addWidget( widget, Qt::AlignLeft );
                    m_layoutWidgetList.push_back( widget );

                    if ( i < data.size() - 1 )
                    {
                        QFrame *line = new QFrame();
                        line->setFrameStyle( QFrame::VLine );
                        line->setAutoFillBackground( false );
                        line->setMaximumHeight( m_height - ( m_headerText->boundingRect().height() + 2 * standardPadding() ) );
                        m_layout->addWidget( line, Qt::AlignLeft );
                        m_layoutWidgetList.push_back( line );
                    }
                }
            }
        }
    }

    // FIXME This should be in engineStateChanged(), but for now it help fixing the bug 210332
    else if ( The::engineController()->phononMediaObject()->hasVideo()
        && The::engineController()->phononMediaObject()->state() != Phonon::BufferingState
        && The::engineController()->phononMediaObject()->state() != Phonon::LoadingState )
    {
        setBusy( false );
        debug() << " VideoclipApplet | Show VideoWidget";
        m_widget->hide();
        m_videoWidget.data()->show();
        m_videoWidget.data()->activateWindow();
        if ( m_videoWidget.data()->inputPaths().isEmpty() )
            Phonon::createPath( const_cast<Phonon::MediaObject*>( The::engineController()->phononMediaObject() ), m_videoWidget.data() );
        if( m_videoWidget.data()->isActiveWindow() )
        {
            QContextMenuEvent e( QContextMenuEvent::Other, QPoint() );
            QApplication::sendEvent( m_videoWidget.data(), &e );
        }
    }
    
    updateConstraints();
}

void 
VideoclipApplet::appendVideoClip( VideoInfo *info )
{
	DEBUG_BLOCK
    QAbstractButton *button = qobject_cast<QAbstractButton *>(QObject::sender() );
    if ( button )
    {
        QStringList lst = button->text().split(" | ");
    
        MetaStream::Track *tra = new MetaStream::Track(KUrl( info->videolink ) );
        tra->setTitle( info->title );
        tra->setAlbum( info->source );
        tra->setArtist( info->artist );
        tra->album()->setImage( info->cover );
        Meta::TrackPtr track( tra );
        //append to the playlist the newly retrieved
        The::playlistController()->insertOptioned(track , Playlist::Append );
    }
}

void
VideoclipApplet::queueVideoClip( VideoInfo *info )
{
    DEBUG_BLOCK
    QAbstractButton *button = qobject_cast<QAbstractButton *>(QObject::sender() );
    if ( button )
    {
        QStringList lst = button->text().split(" | ");
        
        MetaStream::Track *tra = new MetaStream::Track(KUrl( info->videolink ) );
        tra->setTitle( info->title );
        tra->setAlbum( info->source );
        tra->setArtist( info->artist );
        tra->album()->setImage( info->cover );
        Meta::TrackPtr track( tra );
        //append to the playlist the newly retrieved
        The::playlistController()->insertOptioned(track , Playlist::Queue );
    }
}

void
VideoclipApplet::appendPlayVideoClip( VideoInfo *info )
{
    DEBUG_BLOCK
    QAbstractButton *button = qobject_cast<QAbstractButton *>(QObject::sender() );
    if ( button )
    {
        QStringList lst = button->text().split(" | ");
        
        MetaStream::Track *tra = new MetaStream::Track(KUrl( info->videolink ) );
        tra->setTitle( info->title );
        tra->setAlbum( info->source );
        tra->setArtist( info->artist );
        tra->album()->setImage( info->cover );
        Meta::TrackPtr track( tra );
        //append to the playlist the newly retrieved
        The::playlistController()->insertOptioned( track, Playlist::AppendAndPlayImmediately );
    }
}

void
VideoclipApplet::createConfigurationInterface( KConfigDialog *parent )
{
    KConfigGroup configuration = config();
    QWidget *settings = new QWidget;
    ui_Settings.setupUi( settings );
    
    // TODO bad, it's done manually ...
    if ( m_youtubeHQ == true )
        ui_Settings.checkYoutubeHQ->setChecked( true );
    
    parent->addPage( settings, i18n( "Video Clip Settings" ), "preferences-system");
    connect( parent, SIGNAL( accepted() ), this, SLOT( saveSettings( ) ) );
}

void
VideoclipApplet::saveSettings()
{
    DEBUG_BLOCK
    KConfigGroup config = Amarok::config("Videoclip Applet");
    
    m_youtubeHQ = ui_Settings.checkYoutubeHQ->isChecked();
    config.writeEntry( "YoutubeHQ", m_youtubeHQ );
    
    dataEngine( "amarok-videoclip" )->query( QString( "videoclip:youtubeHQ:" ) + QString().setNum( m_youtubeHQ ) );
}

#include "VideoclipApplet.moc"
#include "../../../core/support/SmartPointerList.moc"
